# R_visualizations

Here are a few visualizations from data that is based on water samples that were collected from small streams and ditches running in Southern Ostrobothnia acid sulfate soil area. The time series plot is from a separate obfuscated dataset. The R script that was used for the visualizations is provided in separate textfiles. 
